import sys
import os
import re

import argparse

from neo4jrestclient.client import GraphDatabase

#Create argument parser
parser = argparse.ArgumentParser(description='Populates output from the graph inference program Banjo into Neo4j')
parser.add_argument("-i", "--inputfile", help="The output file from Banjo.", required=True)

parser.add_argument("-s", "--server", help="Optionally specify the hostname. If not, localhost is used.", default="localhost")
parser.add_argument("-p", "--port", help="Optionally specify the port number Default: 7474.", default="7474")

#Parse all arguments
args = parser.parse_args()

#Connect to Neo4j database
gdb = GraphDatabase("http://" + args.server + ":" + args.port + "/db/data/")

#Open input file
banjo_input = open(os.getcwd() + os.sep + args.inputfile )

#Create master graph node - print out it's id so that external programs calling this script can know
graph = gdb.node()
graph.set('type', "graph")
graph.set('algorithm', "BANJO")
print "Graph Node ID: ", graph.id

section = None

numnodes = None

class Node:
	def __init__(self, id):
		self.id = id
		self.edges = {}
		
		self.name = str(id)
		
class Edge:
	def __init__(self, fromid, toid):
		self.fromid = fromid
		self.toid = toid
		self.influence = 0
		
nodes = {}

influence_score_regex = re.compile("""
\(([0-9]*),[0-9]*\)	#First Pair
\s*\-\>\s*	#Second Pair
\(([0-9]*),[0-9]*\) #Influence Score
\s*(\S*)
""",
re.VERBOSE)

label_name_regex = re.compile("""
([0-9]*)
\s*
\[
label="
(.*)
"
\];
""",
re.VERBOSE)

linesread = 0

for line in banjo_input:
	if "Search Statistics" in line:
		section = "Search Statistics"
		continue
	
	if section == "Search Statistics":
		if "Network score:" in line:
			section = "Network"
			continue
			
	if "digraph abstract" in line:
		section = "Graph"
		continue
			
	if section == "Network":
		if numnodes == None:
			numnodes = int( line.strip() )
			print str(numnodes) + " nodes in the data."
			
			for i in range( numnodes ):
				nodes[i] = Node(i)
			
			continue
			
		if linesread < numnodes:
			words = line.strip().split()
			
			childid = int( words[0] )
			connections = int( words[1] )
			
			for i in range( connections ):
				parentid = int( words[2 + i] )
				
				edge = Edge(parentid, childid)
				nodes[parentid].edges[childid] = edge
				
			linesread += 1
					
	if "Influence score for" in line:
		match = influence_score_regex.search( line.strip() )
		words = match.group(1, 2, 3)
		
		fromid = int( words[0] )
		toid = int( words[1] )
		influence = float( words[2] )
		
		nodes[fromid].edges[toid].influence = influence
		
	if section == "Graph":
		match = label_name_regex.search( line.strip() )
		if match != None:
			id = int(match.group(1))
			name = match.group(2)
			nodes[id].name = name
		
print "Pruning nodes..."

for node in nodes.values():
	if len(node.edges) == 0:
		hasparent = False
		for parent in nodes.values():
			if node.id in parent.edges:
				hasparent = True
				break
		
		if hasparent == False:
			del nodes[node.id]
			
print str(  numnodes - len(nodes) ) + " nodes pruned."

"""
for node in nodes.values():
	print node.name,
	print " : ",
	
	for edge in node.edges.values():
		print("(" + str(edge.toid) + ", " + str(edge.influence) + ")" ),
		
	print ""
"""

print "Putting Nodes into Neo4J"
for node in nodes.values():
	node.dbnode = gdb.node()
	node.dbnode.set('name', node.name)
	node.dbnode.set('graphid', graph.id)
	
	graph.relationships.create("CONTAINS", node.dbnode)
	
print "Creating relationships between nodes"
for node in nodes.values():
	for edge in node.edges.values():
		child = nodes[edge.toid]
		node.dbnode.influences(child.dbnode, score=edge.influence, graphid=graphid)
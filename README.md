A preliminary python script for populating Neo4j databases from the output of a Banjo static Bayesian network analysis.

Uses the Neo4J Rest Client for Python - [https://neo4j-rest-client.readthedocs.org/en/latest/](https://neo4j-rest-client.readthedocs.org/en/latest/)

Sample Output:
![Sample Output](http://i.imgur.com/fBly39M.png)